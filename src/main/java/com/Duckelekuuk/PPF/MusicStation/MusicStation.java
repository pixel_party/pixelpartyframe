package com.Duckelekuuk.PPF.MusicStation;

import com.Duckelekuuk.PPF.PixelPartyFrame;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketExtension;
import com.neovisionaries.ws.client.WebSocketFactory;

import javax.net.ssl.SSLContext;

/**
 * @AUTHOR: Duckelekuuk
 * Copyright © 2016, Duco Lindner, All rights reserved.
 */
public class MusicStation {

    private PixelPartyFrame plugin;

    private final String SERVER = "wss://51.255.204.149:8806/MusicServer";
    private final int TIMEOUT = 5000;

    private WebSocket webSocket;
//    private WebSocketServer webSocket;

    public MusicStation(PixelPartyFrame plugin) {
        this.plugin = plugin;

        try {
            webSocket = connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private WebSocket connect() throws Exception {
        // Create a custom SSL context.
        SSLContext context = NaiveSSLContext.getInstance("TLS");
        return new WebSocketFactory()
                .setConnectionTimeout(TIMEOUT)
                .setSSLContext(context)
                .createSocket(SERVER)
                .addListener(new MusicStationListener(plugin, TIMEOUT))
                .addExtension(WebSocketExtension.PERMESSAGE_DEFLATE)
                .connect();
    }

    public void sendText(String username, String command, String value) {
        try {
//            webSocket.sendText("{ \"username\": \"" + username + "\", \"command\" : \"" + command + "\", \"value\" : \"" + value + "\" }");
            webSocket.sendText("{ \"username\": \"" + username + "\", \"command\" : \"" + command + "\", \"value\" : \"" + value + "\" }");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
