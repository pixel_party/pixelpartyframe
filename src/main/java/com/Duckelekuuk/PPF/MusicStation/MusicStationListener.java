package com.Duckelekuuk.PPF.MusicStation;

import com.Duckelekuuk.PPF.PixelPartyFrame;
import com.neovisionaries.ws.client.*;
import org.bukkit.Bukkit;

import java.util.List;
import java.util.Map;

/**
 * @AUTHOR Duco.
 * Description
 */
public class MusicStationListener implements WebSocketListener {

    private PixelPartyFrame plugin;

    private int reconnectTime;
    private boolean shouldReconnect;

    public MusicStationListener(PixelPartyFrame plugin, int reconnectTime) {
        this.plugin = plugin;
        this.reconnectTime = reconnectTime;
        this.shouldReconnect = false;
    }

    public void onStateChanged(WebSocket webSocket, WebSocketState webSocketState) throws Exception {

    }

    public void onConnected(WebSocket webSocket, Map<String, List<String>> map) throws Exception {
        plugin.getMusicStation().sendText("", "plugin_connect", "");
        shouldReconnect = false;
    }

    public void onConnectError(WebSocket webSocket, WebSocketException e) throws Exception {

    }

    public void onDisconnected(WebSocket webSocket, WebSocketFrame webSocketFrame, WebSocketFrame webSocketFrame1, boolean b) throws Exception {
        this.shouldReconnect = true;
        reconnect(webSocket);
    }

    public void onFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onContinuationFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onTextFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onBinaryFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onCloseFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onPingFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onPongFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onTextMessage(WebSocket webSocket, String s) throws Exception {

    }

    public void onBinaryMessage(WebSocket webSocket, byte[] bytes) throws Exception {

    }

    public void onSendingFrame(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onFrameSent(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onFrameUnsent(WebSocket webSocket, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onError(WebSocket webSocket, WebSocketException e) throws Exception {

    }

    public void onFrameError(WebSocket webSocket, WebSocketException e, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onMessageError(WebSocket webSocket, WebSocketException e, List<WebSocketFrame> list) throws Exception {

    }

    public void onMessageDecompressionError(WebSocket webSocket, WebSocketException e, byte[] bytes) throws Exception {

    }

    public void onTextMessageError(WebSocket webSocket, WebSocketException e, byte[] bytes) throws Exception {

    }

    public void onSendError(WebSocket webSocket, WebSocketException e, WebSocketFrame webSocketFrame) throws Exception {

    }

    public void onUnexpectedError(WebSocket webSocket, WebSocketException e) throws Exception {

    }

    public void handleCallbackError(WebSocket webSocket, Throwable throwable) throws Exception {

    }

    public void onSendingHandshake(WebSocket webSocket, String s, List<String[]> list) throws Exception {

    }

    private void reconnect(WebSocket websocket) {
        while (shouldReconnect) {
            try {
                Thread.sleep(reconnectTime);
            } catch (InterruptedException ignored) {
            }
        }
        Bukkit.getServer().getLogger().info("Attemping to reconnect...");
        try {
            websocket.connect();
        } catch (WebSocketException e) {
            e.printStackTrace();
        }
    }
}